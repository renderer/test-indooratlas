package com.testindooratlas;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.indooratlas.android.sdk.IALocation;
import com.indooratlas.android.sdk.IALocationListener;
import com.indooratlas.android.sdk.IALocationManager;
import com.indooratlas.android.sdk.IALocationRequest;

/**
 * Created by tao on 7/15/17.
 */

public class IndoorManager extends ReactContextBaseJavaModule {
  private IALocationManager locationManager;

  public IndoorManager(ReactApplicationContext reactContext) {
    super(reactContext);
  }

  private void sendEvent(ReactContext reactContext,
                         String eventName,
                         @Nullable WritableMap params) {
    reactContext
      .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
      .emit(eventName, params);
  }

  @ReactMethod
  public void ping() {
    WritableMap params = Arguments.createMap();
    sendEvent(getReactApplicationContext(), "pong", params);
  }

  @ReactMethod
  public void init() {
    getCurrentActivity().runOnUiThread(new Runnable() {
      @Override
      public void run() {
        locationManager = IALocationManager.create(getReactApplicationContext());
        locationManager.requestLocationUpdates(IALocationRequest.create(), new IALocationListener() {
          @Override
          public void onLocationChanged(IALocation location) {
            WritableMap params = Arguments.createMap();
            params.putDouble("lat", location.getLatitude());
            params.putDouble("lng", location.getLongitude());
            params.putString("id", location.getRegion().getId());
            sendEvent(getReactApplicationContext(), "locationChanged", params);
          }

          @Override
          public void onStatusChanged(String s, int i, Bundle bundle) {

          }
        });
      }
    });

  }

  @ReactMethod
  public void test(Callback resultCallback) {
    resultCallback.invoke("dcm");
  }

  @Override
  public String getName() {
    return "IndoorManager";
  }
}
