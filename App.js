import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
// import IndoorManager from './IndoorManager';
import MapView from 'react-native-maps';
import WorldWalkable from 'world-walkable';
import test from 'components/test';

const bounds = [
  [20.99655253398431753, 105.85448941658900424],
  [20.99697275090272441, 105.8541715602020048]
];
const obstacleData = [
  {
    type: 'polyline',
    points: [
      { lat: 20.996855378984232, lng: 105.85438795387745 },
      { lat: 20.99683565911204, lng: 105.85438761860134 },
      { lat: 20.99683565911204, lng: 105.85443891584875 },
      { lat: 20.996819382390196, lng: 105.85443992167711 },
      { lat: 20.996820008418, lng: 105.85448384284975 },
      { lat: 20.996854439942766, lng: 105.85448384284975 },
      { lat: 20.996855378984232, lng: 105.85438761860134 },
      { lat: 20.996872281729743, lng: 105.85438862442972 },
      { lat: 20.99687196871595, lng: 105.8543671667576 },
      { lat: 20.9967843248294, lng: 105.85436649620533 },
      { lat: 20.99678495085734, lng: 105.85429809987545 },
      { lat: 20.996875724881377, lng: 105.85429809987545 },
      { lat: 20.996876976936484, lng: 105.85427597165109 },
      { lat: 20.996655989045532, lng: 105.85427530109882 },
      { lat: 20.996655363017044, lng: 105.85429809987545 },
      { lat: 20.996748015204165, lng: 105.85429809987545 },
      { lat: 20.996748015204165, lng: 105.8543671667576 },
      { lat: 20.996659119187914, lng: 105.8543685078621 },
      { lat: 20.996659745216377, lng: 105.85439130663875 },
      { lat: 20.996823764584708, lng: 105.85438728332521 }
    ]
  },
  {
    type: 'polyline',
    points: [
      { lat: 20.99680467073621, lng: 105.85443958640099 },
      { lat: 20.99678745496908, lng: 105.85443958640099 },
      { lat: 20.99678870702494, lng: 105.85447847843172 },
      { lat: 20.996805609778008, lng: 105.85447814315557 }
    ]
  },
  {
    type: 'polyline',
    points: [
      { lat: 20.996766483031898, lng: 105.85438761860134 },
      { lat: 20.99676617001789, lng: 105.8544945716858 },
      { lat: 20.996796219359872, lng: 105.8544945716858 },
      { lat: 20.996796219359872, lng: 105.85448987782001 },
      { lat: 20.99682063444579, lng: 105.85449088364842 },
      { lat: 20.996819695404092, lng: 105.8544808253646 }
    ]
  },
  {
    type: 'polyline',
    points: [
      { lat: 20.996795123811058, lng: 105.85447780787946 },
      { lat: 20.996796219359872, lng: 105.85449473932387 }
    ]
  }
];

const walkable = new WorldWalkable(bounds);
obstacleData.map(obstacle => {
  if (obstacle.type == 'polyline') {
    walkable.addPolyline(
      obstacle.points.map(latlng => [latlng.lat, latlng.lng])
    );
  }
});

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.onRegionChange = this.onRegionChange.bind(this);
    this.state = {
      path: [],
      region: {
        latitude: (bounds[0][0] + bounds[1][0]) / 2,
        longitude: (bounds[0][1] + bounds[1][1]) / 2,
        latitudeDelta: Math.abs(bounds[0][0] - bounds[1][0]),
        longitudeDelta: Math.abs(bounds[0][1] - bounds[1][1])
      }
    };
    this.setTarget = this.setTarget.bind(this);
  }
  componentDidMount() {
    // IndoorManager.init(({ id, lat, lng }) => {
    //   this.setState({
    //     id,
    //     lat,
    //     lng,
    //     region: {
    //       ...this.state.region,
    //       latitude: lat,
    //       longitude: lng
    //     }
    //   });
    //   this.findPath();
    // });
  }
  findPath() {
    // const { target, lat, lng } = this.state;
    // if (target) {
    //   let path = walkable.findPath(
    //     [lat, lng],
    //     [target.latitude, target.longitude],
    //     0
    //   );
    //   path = path.map(([latitude, longitude]) => ({
    //     latitude,
    //     longitude
    //   }));
    //   this.setState({ path });
    // }
  }
  setTarget({ nativeEvent: { coordinate } }) {
    this.setState({
      target: coordinate
    });
    this.findPath();
  }
  onRegionChange(region) {
    this.setState({ region });
  }
  render() {
    const { id, lat, lng, region, path, target } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 3 }}>
          <MapView
            onPress={this.setTarget}
            style={{
              flex: 1,
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
              position: 'absolute'
            }}
            region={region}
            onRegionChange={this.onRegionChange}>
            <MapView.UrlTile urlTemplate="http://192.168.1.17:3000/tile/ct1/{z}/{x}/{y}.png" />
            <MapView.Polyline zIndex={5} coordinates={path} />
            <MapView.Marker
              coordinate={{
                latitude: lat || 37.78825,
                longitude: lng || -122.4324
              }}
            />
            {target && <MapView.Marker coordinate={target} />}
          </MapView>
        </View>
        <View style={{ flex: 1 }}>
          <Text>{test}</Text>
          <Text>{id}</Text>
          <Text>{lat}</Text>
          <Text>{lng}</Text>
        </View>
      </View>
    );
  }
}
