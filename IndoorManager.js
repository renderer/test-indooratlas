import { NativeModules } from 'react-native';
import { DeviceEventEmitter } from 'react-native';

const { IndoorManager } = NativeModules;
const { test, ping, init } = IndoorManager;

export default {
  test,
  ping,
  init: cb => {
    IndoorManager.init();
    DeviceEventEmitter.addListener('locationChanged', cb);
  },
  onLocation: cb => DeviceEventEmitter.addListener('locationChanged', cb),
  onPong: cb => DeviceEventEmitter.addListener('pong', cb)
};
