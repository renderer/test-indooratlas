# Instructions:

## Add IndoorAtlas dependencies to Android project

In `android/build.gradle`

```
allprojects {
    repositories {
        ...
        maven {
            url "http://indooratlas-ltd.bintray.com/mvn-public"
        }
        ...
    }
}
```

In `android/app/build.gradle`

```
dependencies {
    ...
    compile 'com.indooratlas.android:indooratlas-android-sdk:2.4.2@aar'
    ...
}

```

## Edit AndroidManifest.xml

Add permissions outside `<application>` tag

```
    <uses-feature
        android:name="android.hardware.sensor.accelerometer"
        android:required="true"/>
    <uses-feature
        android:name="android.hardware.sensor.compass"
        android:required="true"/>
    <uses-feature
        android:name="android.hardware.sensor.gyroscope"
        android:required="true"/>
    <uses-feature
        android:name="android.hardware.wifi"
        android:required="true"/>

    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.BLUETOOTH"/>
    <uses-permission android:name="android.permission.BLUETOOTH_ADMIN"/>
```

Add API key and secret inside `<application>` tag

```
        <meta-data
            android:name="com.indooratlas.android.sdk.API_KEY"
            android:value="eea8dd19-cc80-447e-a9c6-29f350761c36"/>
        <meta-data
            android:name="com.indooratlas.android.sdk.API_SECRET"
            android:value="y3l4cnvOVN3S8tWi1wMkRUVMkmF0GPZFFf5ERF+pqeaDbmJENV7sjR5YJ0aSgEgMzKej3pB1swmxQpXQ3PYredl+uL9CHMU+7G88xc3kYUFXZ+hTtm4DIhc4lgrHPA=="/>
```

## Add IndoorManager java code

- Change package declaration of `IndoorManager.java`
- Copy `IndoorManager.java` to the same package with `MainApplication.java`

## Add Package java code

- Change package declaration of `MyReactPackage.java`
- Copy `MyReactPackage.java` to the same package with `MainApplication.java`

## Add MyReactPackage to application package list

In `MainApplication.java`

```
    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
        ...
        new MyReactPackage()
      );
    }
```